#SINEVIA PLUGIN SCHEDULER

A plugin that executes commands using a database driven scheduler.

# Installation #

```
#!json

   "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/phplibrary/plugin-scheduler.git"
        }
    ],
    "require": {
        "sinevia/phplibrary/plugin-scheduler": "dev-master"
    },
```

# How to Use? #